@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{ route('home') }}">{{ __('Dashboard') }}</a></div>

                <div>
                    <p> Something Wrong (or You have no permission.) .... </p>
                    <p> (Or) System error occured. Please try again.</p>

                    Go to <a href="{{ route('home') }}">{{ __('Home') }}</a>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('edit') }}/{{Auth::user()->id}}">{{ Auth::user()->name}} Info</a> |
                    <a href="{{ config('mattermost.url') }}" target="_blank">Community</a> |
                    <a href="{{ route('logout') }}">{{ __('Logout') }}</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    ကြိုဆိုပါတယ်

                    @if (Auth::user()->status == 'current_student')
                            သူငယ်ချင်း <b>{{ Auth::user()->name}}({{ Auth::user()->intake_year}})</b>
                    @else
                        @if (Auth::user()->status == 'teacher')
                            Teacher: <b>{{ Auth::user()->name}}</b>
                        @else
                            Alumni: <b>{{ Auth::user()->name}}({{ Auth::user()->intake_year}})</b>
                        @endif
                    @endif
                    ။

                    @if ( Auth::user()->code == NULL )
                        <br />
                        (တာ၀န်ခံ ကိုယ်စားလှယ်များမှ စီစစ်ပြီးလျှင် သက်ဆိုင်ရာ Information များ ဒီနေရာမှာ ပေါ်လာမှာ ဖြစ်ပါတယ်။)
                    @else
                        ကိုယ်ပိုင် Code <b>{{ Auth::user()->code}}</b> ဖြစ်ပါတယ်။ <br/>
                        (Public ဖော်ပြရန် အဆင်မပြေသော အခြေအနေများတွင် ကိုယ်ပိုင် Code ဖြင့် ဖော်ပြပါမည်)

                        @if ( Auth::user()->role == 'agent' )
                            <br/>
                            ~~~~~~~~~~~~<br/>
                            <b>ကိုယ်စားလှယ်အဖြစ် ပါ၀င်လာပေးတဲ့အတွက် ကျေးဇူးတင်ပါတယ်။</b> <br/>
                            ဒီကာလမှာတော့ လုံခြုံရေးကို အထူးဂရုစိုက်ပါ။ <br/>
                            ပိုပြီးကောင်းတဲ့ Community ဖြစ်ဖို့ အကြံပေးပါ။ မိမိနှင့်ပတ်သက်ရာ ကျောင်းသား/ဆရာ/ဆရာမများရဲ့ အသံကို နားစွင့်ပေးပါ။ <br/>
                            အားလုံးအတူတူ ကြိုးစားသွားကြပါစို့။ အရေးတော်ပုံ အောင်ရမည်။ <br/>
                            Myitkyina CU Community ထဲကို <a href="{{ config('mattermost.url') }}" target="_blank">ဒီကနေ</a> ၀င်ပါ။
                        @endif

                    @endif

                    <br/> ~~~~~~~~~~~~ <br/>

                    @if ( Auth::user()->role != 'agent' )
                        Myitkyina CU Community ထဲကို <a href="{{ config('mattermost.url') }}" target="_blank">ဒီကနေ</a> ၀င်ပါ။ Community ထဲမှာတော့ <b>{{ Auth::user()->login_name }}</b> ဖြစ်ပါတယ်။<br>
                        မြစ်ကြီးနား ကွန်ပျူတာ လက်ရှိတက်နေဆဲကျောင်းသား၊ ကျောင်းသားဟောင်း ဆရာ/မများ စုစည်းရာ နေရာ ဖြစ်လို့ လွတ်လပ်ပွင့်လင်းစွာ ပါ၀င်နိုင်ပါတယ်။ ပိုပြီးကောင်းတဲ့ Community ဖြစ်ဖို့လည်း အကြံပေးပါ။
                    @endif

                    @if(Auth::user()->role == 'agent')
                    <h4>All User Information (Total: {{ $users->count() }})</h4>
                    <table>
                        <tr>
                            <th><u>Name</u></th>
                            <th><u>AgentCode</u>&nbsp;&nbsp;</th>
                            <th><u>Role</u>&nbsp;&nbsp;</th>
                            <th><u>Verified?</u></th>
                        </tr>

                        @foreach ($users as $user)
                        <tr>
                            <td><a href="{{ route('edit') }}/{{$user->id}}">{{ $user->name}}</a>&nbsp;</td>
                            <td>{{ $user->agent_passcode}}</td>
                            <td>{{ $user->role}}</td>
                            <td>&nbsp;&nbsp;<a href="{{ route('edit') }}/{{$user->id}}">
                                @if ($user->verification == config('const.Verification.OK'))
                                    &#10004;OK
                                @else
                                    @if ($user->verification == config('const.Verification.NOT_YET'))
                                        NotYet
                                    @else
                                        &#10060;<b style="color:red">NG</b>
                                    @endif
                                @endif

                            </a></td>
                        </tr>
                        @endforeach
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

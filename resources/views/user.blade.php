@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><a href="{{ route('home') }}">{{ __('Dashboard') }}</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->role == 'agent' || Auth::user()->id == $user->id)

                    <b>{{ $user->name}} ({{ $user->status}} | {{ $user->intake_year}})</b>
                    @if($user->code == NULL)
                    <u style="color:red">(အတည်မပြုရသေး)</u>
                    @endif
                    @if($user->verification == config('const.Verification.NG') )
                    <u style="color:red">(စစ်ဆေးရန်လို)</u>
                    @endif
                    @if($user->flag == config('const.Flag.DELETED') )
                    <u style="color:red">(DELETED)</u>
                    @endif
                    @if($user->verification == config('const.Verification.OK') )
                    &#10004;
                    @endif
                    <br>
                    <br>
                    <table align="center">
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td>{{ $user->name}}</td>
                        </tr>
                        <tr>
                            <td>Stauts</td>
                            <td>:</td>
                            <td>{{ $user->status}}</td>
                        </tr>
                        <tr>
                            <td>Intake Year</td>
                            <td>:</td>
                            <td>{{ $user->intake_year}}</td>
                        </tr>
                        @if ($user->status == 'teacher')
                        <tr>
                            <td>Department</td>
                            <td>:</td>
                            <td>{{ $user->department}}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>E-Mail</td>
                            <td>:</td>
                            <td>{{ $user->email}}</td>
                        </tr>
                        <tr>
                            <td>Phone No</td>
                            <td>:</td>
                            <td>{{ $user->phone_no}}</td>
                        </tr>
                        <tr>
                            <td>Registered Passcode</td>
                            <td>:</td>
                            <td>{{ $user->agent_passcode}}</td>
                        </tr>
                        @if (Auth::user()->role == 'agent')
                        <tr>
                            <td>မှတ်ချက် </td>
                            <td>:</td>
                            <td>{{ $user->note }}</td>
                        </tr>
                        @if ($user->last_confirmed_by)
                        <tr>
                            <td>အတည်ပြု(or)စီစစ်ခဲ့သူ</td>
                            <td>:</td>
                            <td>{{ $user->last_confirmed_by->agent_passcode }}</td>
                        </tr>
                        @endif
                        @endif
                        <tr>
                            <td colspan="3">~~~~~~~~~~~~~~~~~~~~~~~~~~~</td>
                        </tr>
                        <tr>
                            <td>Current Role</td>
                            <td>:</td>
                            <td>{{ $user->role}}</td>
                        </tr>
                        @if ($user->role == 'agent')
                        <tr>
                            <td>His/Her Agent Code</td>
                            <td>:</td>
                            <td>{{ $user->agent->agent_passcode }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td>Community Status</td>
                            <td>:</td>
                            <td>{{ $user->community_status}}</td>
                        </tr>
                        <tr>
                            <td>Community Id</td>
                            <td>:</td>
                            <td>{{ $user->login_name}}</td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center"><a href="{{ config('mattermost.url') }}" target="_blank">Goto Myitkyina CU Community</a></td>
                        </tr>
                        <tr>
                            <td colspan="3">~~~~~~~~~~~~~~~~~~~~~~~~~~~</td>
                        </tr>
                    </table>
                    <br/>
                    @if(Auth::user()->role == 'agent')
                    <form method="POST" action="{{ route('edit') }}">
                        @csrf
                        <input type="hidden" value="{{ $user->id }}" name="id">
                        <div class="form-group row">
                            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('အဖွဲ့၀င်') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" id="role" name="role">
                                        <option value="user"  @if ($user->role == 'user' || old('role') == 'agent' ) selected @endif>{{ __('Member (ပုံမှန်အဖွဲ့၀င်)') }}</option>
                                        <option value="agent" @if ($user->role == 'agent' || old('role') == 'agent' ) selected @endif>{{ __('Agent (ကိုယ်စားလှယ်)') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="agent_passcode" class="col-md-4 col-form-label text-md-right">{{ __('ကိုယ်စားလှယ် ကိုယ်ပိုင် code') }}</label>

                            <div class="col-md-6">
                                <input id="agent_passcode" type="text" class="form-control @error('agent_passcode') is-invalid @enderror" name="agent_passcode" value="{{ old('agent_passcode') }}" autocomplete="agent_passcode">

                                @error('agent_passcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="note" class="col-md-4 col-form-label text-md-right">{{ __('မှတ်ချက် (option)') }}</label>

                            <div class="col-md-6">
                                <input id="note" type="text" class="form-control @error('note') is-invalid @enderror" name="note" value="{{ old('note') }}" autocomplete="note">

                                @error('note')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <input type="radio" name="verification" value="{{ config('const.Verification.OK') }}" @if (old('verification') == config('const.Verification.OK') || $user->verification == config('const.Verification.OK') || $user->verification == NULL) checked @endif> အိုကေ
                                <input type="radio" name="verification" value="{{ config('const.Verification.NG') }}" @if (old('verification') == config('const.Verification.NG') || $user->verification == config('const.Verification.NG')) checked @endif> ထပ်စစ်ရန်လို
                                <input type="radio" name="verification" value="{{ config('const.Flag.DELETED') }}"  @if (old('verification') == config('const.Flag.DELETED')) checked @endif> ဖျက်မည်
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <br/>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    <table>
                    @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

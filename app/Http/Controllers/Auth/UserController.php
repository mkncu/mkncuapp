<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Config;
use GuzzleHttp\Client;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get($userId)
    {
        try {
            $dbArrayData = User::where('id', $userId)->get();
            if($dbArrayData->count()==0){
                return view('logic_error');
            }
            $user = $dbArrayData[0];
            $dbArrayData = Agent::where('user_id', $userId)->get();
            if($dbArrayData->count()>0){
                $user->agent = $dbArrayData[0];
            }
            $dbArrayData = Agent::where('user_id', $user->updated_by)->get();
            if($dbArrayData->count()>0){
                $user->last_confirmed_by = $dbArrayData[0];
            }
        } catch (\Exception $e) {
            return view('logic_error');
        }
        return view('user', ['user' => $user]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'agent_passcode' => 'required_if:role,agent|nullable|min:5|max:7|unique:agents,agent_passcode',
            'note' => 'required_if:verification,'.config('const.Verification.NG').'|required_if:verification,'.config('const.Flag.DELETED').'|max:255',
        ]);
        try {
            $dbArrayData = User::where('id', $request->id)->get();
            if($dbArrayData->count()==0){
                return view('logic_error');
            }
            $user = $dbArrayData[0];

            $verification = config('const.Verification.OK');
            $role = $request->role;
            if($request->verification == config('const.Verification.NG')) {
                $verification = config('const.Verification.NG');
                $role = 'user';
            }
            if($request->verification == config('const.Flag.DELETED')) {
                $verification = config('const.Flag.DELETED');
            }

            if($user->code == NULL && $verification == config('const.Verification.OK')) {

                $code = strtoupper(substr($user->status,0,1));
                if($user->status == 'teacher') {
                    $statusCount = User::whereNotNull('code')->
                                where('status', '=', $user->status)->get()->count();
                    $code = $code.sprintf("%'.04d\n", $statusCount+1);
                } else {
                    $statusCount = DB::select("select * from users where code is not null and status in (?, ?)", array('alumni','current_student'));
                    $statusCount = count($statusCount);
                    $code = $code.sprintf("%'.04d\n", $statusCount+1);
                }

                User::where('id', $user->id)
                ->update([
                        'role' => $role,
                        'verification' => $verification,
                        'code' => $code,
                        'note' => $request->note,
                        'flag' => config('const.Flag.ALIVE'),
                        'updated_by' => Auth::user()->id
                    ]);

            } else {
                if($verification == config('const.Flag.DELETED')){
                    User::where('id', $user->id)
                    ->update([
                            'note' => $request->note,
                            'flag' => config('const.Flag.DELETED'),
                            'updated_by' => Auth::user()->id
                        ]);

                    $client = new Client();
                    $response = $client->delete(config('mattermost.deactivate') . $user->community_system_id, [
                        'headers' => [
                            'Authorization' => 'Bearer '. config('mattermost.token')
                        ]
                    ]);

                } else {
                    User::where('id', $user->id)
                        ->update([
                            'role' => $role,
                            'verification' => $verification,
                            'note' => $request->note,
                            'flag' => config('const.Flag.ALIVE'),
                            'updated_by' => Auth::user()->id
                        ]);

                    if($user->community_system_id){
                        if($verification == config('const.Verification.NG')) {
                            $client = new Client();
                            $response = $client->delete(config('mattermost.deactivate') . $user->community_system_id, [
                                'headers' => [
                                    'Authorization' => 'Bearer '. config('mattermost.token')
                                ]
                            ]);
                        } else {
                            $client = new Client();
                            $endpoint = str_replace("user_id", $user->community_system_id, config('mattermost.activate'));
                            $response = $client->put($endpoint, [
                                'headers' => [
                                    'Accept' => 'application/json',
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer '. config('mattermost.token')
                                ],
                                'json' => [
                                    'active' => true
                                ],
                            ]);
                        }
                    }
                }
            }

            if($role == 'agent') {
                Agent::create([
                    'user_id' => $user->id,
                    'agent_passcode' => $request->agent_passcode,
                    'created_by' => Auth::user()->id,
                    'updated_by' => Auth::user()->id,
                ]);
            }
        } catch (\Exception $e) {
            return view('logic_error');
        }

        return redirect('home');
    }

    public function home()
    {
        try {
            $users = User::where('status', '!=', 'system_admin')->
                    where('flag', '==', config('const.Flag.ALIVE'))->get()->sortByDesc('verification');
            return view('home', ['users' => $users]);
        } catch (\Exception $e) {
            return view('logic_error');
        }
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Rules\AgentPasscode;
use App\Rules\UserName;
use Auth;
use Hash;
use Config;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function register()
    {
      return view('auth.register');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:30', new UserName($request->intake_year, $request->status)],
            'email' => 'required|string|email|max:255|unique:users',
            'password' => [
                    'required', 'confirmed', 'min:8',
                    'regex:/[a-z]/',
                    'regex:/[A-Z]/',
                    'regex:/[0-9]/'
                ],
            'password_confirmation' => 'required',
            'intake_year' => 'required|integer|min:1900|max:2021',
            'phone_no' => 'numeric|min:8',
            'agent_passcode' => ['required', 'min:5', new AgentPasscode()],
            'department' => 'required_if:status,teacher',
        ]);
        try {
            $loginName = strtolower(str_replace(' ', '', $request->name));

            if($request->status == 'teacher') {
                $loginName = $loginName . "_tchr";
            } else {
                $loginName = $loginName .  "_" . $request->intake_year;
            }

            $mattermostUserAPI = config('mattermost.userUrl');
            $mattermostTeamAPI = config('mattermost.teamUrl');
            $mattermostAccessToken = config('mattermost.token');
            $mattermostTeamId = config('mattermost.teamId');

            $client = new Client();
            $response = $client->post($mattermostUserAPI, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$mattermostAccessToken
                ],
                'json' => [
                    'email' => $request->email,
                    'username' => $loginName,
                    'password' => $request->password
                ],
            ]);
            $jsonRes = json_decode($response->getBody()->getContents());

            $communityUserId = $jsonRes->id;
            $response = $client->post($mattermostTeamAPI, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$mattermostAccessToken
                ],
                'json' => ['team_id' => $mattermostTeamId, 'user_id' => $communityUserId],
            ]);

            User::create([
                'name' => $request->name,
                'password' => Hash::make($request->password),
                'email' => $request->email,
                'phone_no' => $request->phone_no,
                'login_name' => $loginName,
                'intake_year' => $request->intake_year,
                'department' => $request->department,
                'agent_passcode' => $request->agent_passcode,
                'status' => $request->status,
                'ip_address' => $request->ip(),
                'verification' => config('const.Verification.NOT_YET'),
                'community_system_id' => $communityUserId,
                'community_status' => 'Joined.'
            ]);

        } catch (\Exception $e) {
            $errorMessage = 'Please try again. System error occur.';
            return view('logic_error', ['errorMessage' => $errorMessage]);
        }

        return redirect('home');
    }

    public function login()
    {
        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if(Auth::user()->flag == config('const.Flag.ALIVE'))
            {
                return redirect()->intended('home');
            } else {
                Auth::logout();
                return redirect('login')->with('error', 'Account မှားယွင်းနေပါတယ်။ ကိုယ်စားလှယ်နှင့် ဆက်သွယ်ပါ။');
            }
        }

        return redirect('login')->with('error', 'Oppes! You have entered invalid credentials');
    }

    public function logout() {
      Auth::logout();

      return redirect('login');
    }

}

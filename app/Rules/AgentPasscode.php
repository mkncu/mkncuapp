<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Agent;

class AgentPasscode implements Rule
{
    /* Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $agentCode = substr($value, 0, 5);
        $agentCount = Agent::where('agent_passcode', 'LIKE', $agentCode.'%')->get()->count();

        if($agentCount > 0) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'ကိုယ်စားလှယ် ဆီက Invitation Code ကိုထည့်ပါ။';
    }
}
